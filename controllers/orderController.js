'use strict';
var express = require('express');
var router = express.Router();
var app = express();
const fetch = require("node-fetch");
const http = require('http');
const axios = require('axios');


var Order = require('../models/orderModel.js')
var Product = require('../models/productsModel.js')

var pr = require('../controllers/productController');

const appUrl = 'http://185.224.139.140:3002/'
// const appUrl = 'http://192.168.1.104:3001/'

exports.addNewOrder = function(req,res){

    var new_ord = new Order(req.body);

    Order.addOrder(new_ord, function(err, ord) {
      if (err)
        res.send(err);
      

        axios.get(appUrl+'user/'+req.body.userId,)
            .then(resp => {

                var accountSid = 'AC71f6015942073f4a55b104540ad443a6'; // Your Account SID from www.twilio.com/console
                var authToken = 'c71dc84d04cc6a3471678f167fe798c8';   // Your Auth Token from www.twilio.com/console
            
                var twilio = require('twilio');
                var client = new twilio(accountSid, authToken);
            
                client.messages.create({
                    body: 'Romastera Yeni Sipariş gelmiştir. Sipariş Sahibi : '+ resp.data[0].name + ' ' + resp.data[0].surname,
                    to: 'whatsapp:+905523822040',  // Text this number
                    from: 'whatsapp:+14155238886' // From a valid Twilio number
                    // from: 'whatsapp:+19093142544' // From a valid Twilio number
                })
                .then((message) => console.log(message))  

            }).catch((err) => {
                console.log('ERRRORRR')
                console.error(err);
                console.log('ERRRORRR')
            });
       
      
    res.json(ord);
    });

}

async function defabc(usr) {
    const promises = []
    usr.forEach(updatedNode => {
      JSON.parse(updatedNode.orderDetail).forEach((orderProduct) => {
        promises.push(axios.get(`${appUrl}productDetail/${orderProduct.id}`))
      })
    })
    const resArray = await Promise.all(promises)
    const abc = resArray.map(response => {
        let orderProduct = response.data
        let opc = JSON.parse(orderProduct[0].productTypes);
      opc.map(opt => {
        JSON.parse(response.data[0].productTypes).map(node => {
          if(node.id == opt.id){
            delete opt['quantity']
            delete node['quantity']
            opt.quota = node.quota
          }
        }) 
        return opt;
      })
    })
  }


exports.getPendingOrders = function(req,res){
    Order.getPendingOrders(function(err, usr){
        if(err)
            res.send(err);
            
            // defabc(usr).then(function(result) {
            //     console.log(result) // "Some User token"
            //  })
            // res.send(defabc(usr))


            // let prms = Promise.all(usr.map(updatedNode=>JSON.parse(updatedNode.orderDetail).map(orderProduct=>
            //     axios.get(appUrl +'productDetail/'+orderProduct.id))))

            //     prms.then(res => {
            //         console.log(res)
            //     })

            // let abc = usr.map(updatedNode=> {
            //     JSON.parse(updatedNode.orderDetail).map(orderProduct=> {
            //         axios.get(appUrl +'productDetail/'+orderProduct.id)
            //             .then(function (response) {
            //                 orderProduct.productTypes.map(opt => {
            //                     JSON.parse(response.data[0].productTypes).map(node => {
            //                         if(node.id == opt.id){
            //                             delete opt['quantity']
            //                             delete node['quantity']
            //                             opt.quota = node.quota
            //                         }
            //                     }) 
            //                     // console.log(opt)
            //                     return opt;
            //                 })
            //             })
            //             // console.log(orderProduct)
            //             return orderProduct
            //         })
            //         // console.log(updatedNode)
            //         return updatedNode
            //     })
               res.send(usr);
    });
}

const swappendingOrdersRealQuota = (pendingOrders) => {

    pendingOrders.map(updatedNode=> {
        let pends = JSON.parse(updatedNode.orderDetail)

    pends.map(orderProduct=> {
        axios.get(appUrl + 'productDetail/'+orderProduct.id)
          .then(function (response) {

            let dbProd = response.data[0]
            let pTypes = JSON.parse(dbProd.productTypes)

            console.log(pTypes)
             
            orderProduct.productTypes.map(opt => {
                pTypes.map(node => {
                    if(node.id == opt.id){
                        delete opt['quantity']
                        delete node['quantity']
                        opt.quota = node.quota
                    }

                }) 
            })
          })
          .catch(function (error) {
            console.log(error);
          })
          .then(function () {
            // always executed
          }); 
        
    })
})


return pendingOrders;

}


exports.listOrdersByUserId = function(req,res){
    Order.listOrderOfUser(req.params.id, function(err,ords){
        if (err)
        res.send(err);
          res.send(ords);
    });


}

exports.removeOrder = function(req,res){
    Order.removeOrderById(req.params.id, function(err,ords){
        if (err)
        res.send(err);
          res.send(ords);
    });


}


exports.updateOrder = function(req,res){

    // maintainProducts(pid);
    const obj = JSON.parse(req.body.orderDetail)
    // console.log(obj)
    // res.json(req.body)

    obj.map(p =>{
        // updateProductsQuota(p)


        axios.get(appUrl+'productDetail/'+p.id,)
            .then(resp => {

                let pd = resp.data
                let pdT = JSON.parse(pd[0].productTypes)

                let totalQuota = 0
                p.productTypes.map(updatedNode=> {
                    pdT.map(dbProd => {
                        if(updatedNode.id == dbProd.id){
                        console.log('----')
                        console.log(updatedNode.value)
                        // TODO burasını hep dbden çekmemiz gerekiyor
                        // delete dbProd['quantity']
                        updatedNode.value = (updatedNode.value == "" ? 0 : updatedNode.value)

                        dbProd.quota = parseInt(dbProd.quota) - parseInt(updatedNode.value)
                        // console.log(totalQuota)
                        totalQuota += parseInt(dbProd.quota);
                        // console.log(totalQuota)
                    }
                })


            })

            const dataZ = {
                productTypes: JSON.stringify(pdT),
                quantity : totalQuota
            };


 
        axios.put(appUrl+'products/'+ p.id, dataZ)
            .then((res) => {
                console.log(`Status: ${res.status}`);
                console.log('Body: ', res.data);
        }).catch((err) => {
            console.error(err);
        });
        
    }).catch((err) => {
        console.error(err);
    });


      

        
    })


    Order.updateOrder(req.params.id, new Order(req.body), function(err,task){
        if (err)
            res.send(err);
        res.json(task);
    });
};


const updateProductsQuota = (product) => {

    // console.log(product.id)

    http.get( appUrl + 'productDetail/'+product.id, (res) => {
            //   console.log('statusCode:', res.statusCode);
            //   console.log('headers:', res.headers);

    
            res.on('data', (d) => {
                // console.log('(/////')
                let pd = JSON.parse(d)




                console.log('**********')
                console.log('**********')
                console.log('**********')
                console.log(pd[0].productTypes)
                console.log('**********')
    console.log('**********')
    console.log('**********')



    pd[0] = product.productTypes;


                console.log(pd[0].productTypes)
                console.log('**********')
                console.log('**********')
                console.log('**********')
                // console.log(pd[0].productTypes)
                // let parsed = pd.productTypes
                // console.log(parsed)
                // direk mergele


                
              });


            }).on('error', (e) => {
                console.error('ERROR',e);
            });
                // console.log(typeof )
                // let pTypes = 

                // console.log(JSON.stringify(product))
                // console.log('//////')
                // console.log('//////')
                // console.log('//////')
                // console.log('//////')
                // console.log('//////')
                // console.log(JSON.stringify(pd))
                // console.log('A')

                // product.productTypes.map(p=> {
                //     pd[0].map(newQuotad =>{
                //         console.log(newQuotad.productType)
                //         console.log('//////')
                //         console.log('//////')
                //         console.log('//////')
                //         console.log('//////')
                //         // console.log(newQuotad)
                //         console.log('//////')
                //         console.log('//////')
                //         console.log('//////')
                //         console.log('//////')
                //         console.log('//////')

                //         // console.log(newQuotad.id, p.id)
                //         // if(newQuotad.id == p.id)
                //         //     console.log(p.productType, newQuotad.productType)

                //     })
                // })
                // console.log('fin')

                // pd.map(updated => {
                //     product.forEach((prod) => {
                //         if(updated.id == prod.id)
                //             console.log(prod.productType, updated.productType)

                //     })

                // })

                    // console.log(JSON.stringify(pd))
                    // console.log('////////')
                    // console.log('////////')
                    // console.log('////////')
                    // console.log('////////')
                    // console.log('////////')
                    // console.log('////////')
                    // console.log(JSON.stringify(product))

                    
                //     pd.map(rP => {
                //     if(product.id == rP.id)
                //         console.log('VAR')
                //         // console.log(product, rP)
                //     else 
                //         console.log('YOK')
                // })
                
                // console.log(JSON.parse(d))


                // console.log('(/////')
                // process.stdout.write(d);



}




const maintainProducts = (products) => {
    products.map(p =>{
        updateProductsQuota(p)
    })
}

      