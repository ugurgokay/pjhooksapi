'use strict';
module.exports = function(app) {
  var cats = require('../controllers/categoryController');
  // var cf = require('../controllers/categoryFeaturesController');
  var pr = require('../controllers/productController');
  var us = require('../controllers/usersController');
  var or = require('../controllers/orderController');
  var auth = require('../controllers/authController');
  var nw = require('../controllers/newsController');
  // Main Categories Routes
  const checkAuth = require('../middleware/checkauth');


  app.route('/categories')
    .get(cats.list_main_categories)
    .post(cats.create_a_category);

    app.route('/subCategories')
      .get(cats.list_sub_categories);
   
   app.route('/categories/:taskId')
    // TODO // .get(cats.read_a_task) // getProductsByCategoryId
    .get(cats.list_sub_categories_by_id)
    .put(cats.update_a_category)
    // .put(cats.delete_a_category);
   


    // Categories Features Routes
    // app.route('/categoriesFeatures')
    //   .get(cf.list_all_category_features)
    //   .post(cf.create_a_category_feature); 
    



    //Product
    app.route('/products')
      // getProductsByCategoryAndALLPRODUCTS
      .get(checkAuth,pr.list_all_products)
      .post(pr.create_a_product);
      // putUpdateProduct


      

    //getProductsBySubCategory | update product Info
    app.route('/products/:productId')
      .get(checkAuth,pr.list_products_by_category_id)
      .put(pr.update_product_info);

      app.route('/_prsds/:productId')
      // getProductsByCategoryAndALLPRODUCTS
      .get(pr.list_products_by_category_id)
      .put(pr.update_product_info);

      app.route('/_prsdzz/:productId')
      // getProductsByCategoryAndALLPRODUCTS
      .put(pr.update_product_info_price);

      app.route('/lastProduct')
      .get(pr.get_last_product)

    // getProductDetail
    app.route('/productDetail/:productId')
      .get(pr.get_product_detail_by_id)
      .put(pr.remove_product_by_id); // ürünü pasifleştirme

    // createNewUser
    app.route('/user')
      .post(us.create_a_user);

    // get UserDetail
    app.route('/user/:id')
      .get(us.get_user_by_id);
      

    // get All Users
    app.route('/users')
      .get(checkAuth,us.get_all_users);

    //updateLastOnlineOfUSer
    app.route('/lastOnline/:id')
      .put(us.update_last_online_of_user)


    // activate | deactivate user  
    app.route('/updateUserStatus/:id')
      .put(us.update_user_status_by_id)


    // getPendingOrders | addANewOrder
    app.route('/order')
      .post(or.addNewOrder)
      .get(checkAuth,or.getPendingOrders)


    // listUsersOrders | updateOrderByID
    app.route('/orders/:id')
      .get(or.listOrdersByUserId)
      .put(or.updateOrder)

    app.route('/removeOrder/:id')
      .put(or.removeOrder)

    // AUTH
    app.route('/auth/:un/:pw')
      .get(auth.login);

    // listNews | addNews
    app.route('/news')
      .get(nw.list_news)
      .post(nw.add_news);


    app.route('/showDemoAccount')
      .get(auth.demoAcc)
  




      app.route('/verifyAdmin')
      .post(auth.verifyAdmin);


      app.route('/checkUserValid/:id')
      .get(us.check_user_is_valid)
      // app.route('/usersz')
      // .get(checkAuth,nw.list_news);


      app.route('/sendWP')
      .get(auth.sendSMS)



    };
    // c'est fini