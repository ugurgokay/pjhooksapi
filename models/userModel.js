'use strict';
var sql = require('../db.js');
var uuid =require('uuid');


//userObject.create
var User = function (usr){
    this.name = usr.name;
    this.surname = usr.surname;
    this.countryCode = usr.countryCode;
    this.eMail = usr.eMail;
    this.password = usr.password;
    this.phoneNumber = usr.phoneNumber;
    this.location = usr.location;
    this.status = usr.status;
    this.passwordAttempt = usr.passwordAttempt;
    this.deviceType = usr.deviceType;
    this.lastOnlineRand = usr.lastOnlineRand;
};

User.createUser = function(usr, result){
    // this.id = uuid.v4();
    usr.id = uuid.v4();
    usr.passwordAttempt = 0;
    usr.lastOnlineRand = 0;
    sql.query("INSERT INTO  user set ? ", usr, function(err, res){
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            // console.log(res.insertId);
            result(null, usr.id);
        }
    });
};

User.getAllUSers = function(result){

    sql.query("Select * from user WHERE status != 2 ORDER BY status", function (err, res) {

        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
         result(null, res);
        }
    });  
}

User.GetUserById = function(id,result){
    sql.query("SELECT id,name,surname,countryCode,eMail,phoneNumber,status FROM user where id = ?", id, function(err,res){
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
         result(null, res);
            // console.log(res);
        }
    })
};

User.UpdateLastOnlineByUserId = function(id, task, result){
    let t = parseInt(Math.random()*100)
    sql.query("UPDATE user SET lastOnlineRand = ? WHERE id = ?", [t,id], function (err, res) {
        if(err) {
            console.log("error: ", err);
                result(null, err);
            }
            else{   
                console.log(res)
            result(null, res);
                }
            }); 

};

User.setStatusOfUser = function(id, type,result){
    let pw = 'R'+parseInt(Math.random()*1000000)

    console.log("UPDATE user SET status = "+type.status+" AND password = "+pw+" WHERE id = "+ id )
    console.log(type.status)
    sql.query("UPDATE user SET status = ? , password = ? WHERE id = ?", [type.status,pw,id], function (err,res){
        if(err)
        result(null,err)
        else{
            result(null,pw)
        }
    });
}

User.isUserValid = function(id,result){
    sql.query("SELECT count(id) as isValid FROM user WHERE id = ? AND status = 1", [id], function(err,res){
        if(err)
        result(null,err)
        else{
             result(null,res)
        }

    });
}

module.exports = User;